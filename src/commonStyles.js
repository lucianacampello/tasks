export default {
    fontFamily: 'Lato',
    colors: {
        default: '#1631b3',
        today: '#b13b44',
        secundary: '#FFF',
        mainText: '#222',
        subText: '#555'
    }
}